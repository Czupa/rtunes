'use strict';
var jrTunes = angular.module('jrTunes', [
  'ngRoute',
  'jrtunesControllers',
  'jrtunesServices',
  'jrtunesFilters',
  'jrtunesDirectives']);

jrTunes.config(function($routeProvider) {
  $routeProvider
    .when('/home',
    {
      controller: 'SongListCtrl',
      templateUrl:'/home'
    }
    )
    .when('/about',
    {
      templateUrl: "about"
    }
    )
    .otherwise({redirectTo: '/home'});
});
