# encoding: utf-8
require 'yaml'
require_relative 'song'
require_relative 'library'

class Browser
  attr_reader :albums, :songs, :song_hash

  def initialize
    set_variables
  end

  def set_variables(lib = nil)
    @lib = lib || Library.new
    [:config, :songs, :artists, :albums, :genres].each do |sym|
      instance_variable_set("@#{sym}", @lib.send(sym))
    end
  end

  def reload_music
    Song.res_count
    @lib.load_songs
    set_variables(@lib)
  end

  def change_music_dir(dir)
    config = {}
    config[:dir] = dir
    Song.res_count
    set_variables Library.new(dir)
  end

  def load_from_cfg(sth)
    @config[sth]
  end

  def remove_song(id)
    song = @songs[id - 1]
    song.delete
    @songs.delete @songs[id - 1]
    [:album, :artist, :genre].each do |tag|
      delete_from_tag tag, song
    end
  end

  def delete_from_tag(tag, song)
    v = instance_variable_get("@#{tag}s")
    s_val = song.public_send(tag)
    unless s_val.empty?
      v[s_val][:songs].delete song.id
      v.delete s_val if v[s_val][:songs].empty?
    end
  end

  def change_song_tag(song, tag, value)
    tag = tag.to_sym
    arrayed = [:album, :artist, :genre]
    song.change_tag(tag, value)
    @songs[song.id - 1] = song
    reload_music if arrayed.include? tag
  end

  #def change_tag_in_array(song, tag, value)
    #tag = tag.to_sym
    #array = case tag
      #when :album
        #@albums
      #when :artist
        #@artists
      #when :genre
        #@genres
      #else
        #self.send "#{tag}s"
    #end
    #array[song.send(tag)][:songs].delete song.id
    #array.delete song.send(tag) if array[song.send(tag)][:songs].empty?
    #array[value.to_sym] ||= {}
    #array[value.to_sym][:songs] ||= []
    #array[value.to_sym][:songs] << song.id
  #end

  #def save
    #@config[:genres] = @genres
    #@config[:albums] = @albums
    #@config[:artist] = @artists
    #@config[:songs] = @songs
    #@config[:song_hash] = @song_hash
    #File.open(File.dirname(__FILE__) + '/config.yml', 'w') do |f|
      #f.write @config.to_yaml
    #end
  #end

  def to_hash
    [:albums, :artists, :genres].each_with_object({}) do |i, h|
      h[i] = instance_variable_get("@#{i}".freeze)
    end.tap  { |h| h[:songs] = @songs.map(&:to_hash) }
  end
end
