# encoding: utf-8
require 'sinatra'
require 'haml'
require 'json'
require_relative 'browser'
class WebGui < Sinatra::Base
  BOOT_TIME = Time.now
  BROWSER = Browser.new
  set :protection, false

  before method: :get do
    @start_time = Time.now
  end

  get '/' do
    haml :index
  end

  get '/library' do
    # BROWSER.reload_music
    content_type :json
    BROWSER.to_hash.to_json
  end

  get '/home' do
    haml :home
  end

  not_found do
    redirect '/'
  end

  get '/about' do
    @quotes = ['30 mega ramu i się toczy.', 'Pan Bóg zamyka drzwi i okna, ale otwiera okna.', 'Ale butów to ty nie przebrałeś.',
     'Click. Boom. Amazing!', 'Tego chcą masy!', 'I like trains!', 'Ja wiem lepiej od ciebie.', 'Jestem bystra.', 'Siódemki za to nie dostaniesz']
     haml :about
   end

   get '/public/albums/:image' do
    redirect '/albums/:image'
  end

  post '/change_folder' do
    BROWSER.change_music_dir(params[:path])
    redirect '/'
  end

  get '/reload' do
    BROWSER.reload_music
    redirect '/'
  end

  post '/edit_song' do
    if params['id'].to_i > 0
      song = BROWSER.songs[params[:id].to_i - 1]
      params.delete 'id'
      params['artist'] = params.delete 'artist_name'
      params.each do |k, v|
        if song.send(k.to_sym) != v && k.to_sym != :genre
          BROWSER.change_song_tag(song, k, v)
        elsif k.to_sym == :genre
          if Song::GENRES[v.to_i] != song.genre
            BROWSER.change_song_tag(song, k, v)
          end
        end
      end
    end
    redirect '/'
  end

  post '/remove_song' do
    if params['id'].to_i > 0
      BROWSER.remove_song(params['id'].to_i)
      # BROWSER.reload_music
    end
    redirect '/'
  end

  get '/file/*' do |path|
    send_file "/#{path}"
  end
end

# WebGui.run!
