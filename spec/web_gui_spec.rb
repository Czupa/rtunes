# encoding: utf-8
require_relative('../lib/web_gui')
require 'rack/test'
require 'sinatra'

RSpec.configure do |config|
  config.include Rack::Test::Methods
end

def app
  WebGui
end
describe "get /" do
  it "should display the homepage" do
    get "/"
    expect(last_response).to be_ok
    expect(last_response.body).to match '<title>jrTunes - Się toczy się gra!</title>'
  end
end