'use strict';
var jrtunesDirectives = angular.module('jrtunesDirectives', []);
jrtunesDirectives.directive('editButton', function() {
  function link(scope, element, attrs) {
    element.bind('click',function() {
    var genres = [
      'Blues', 'Classic Rock', 'Country', 'Dance', 'Disco', 'Funk',
      'Grunge', 'Hip-Hop', 'Jazz', 'Metal', 'New Age', 'Oldies',
      'Other', 'Pop', 'R&B', 'Rap', 'Reggae', 'Rock',
      'Techno', 'Industrial', 'Alternative', 'Ska', 'Death Metal', 'Pranks',
      'Soundtrack', 'Euro-Techno', 'Ambient', 'Trip-Hop', 'Vocal', 'Jazz+Funk',
      'Fusion', 'Trance', 'Classical', 'Instrumental', 'Acid', 'House',
      'Game', 'Sound Clip', 'Gospel', 'Noise', 'AlternRock', 'Bass',
      'Soul', 'Punk', 'Space', 'Meditative', 'Instrumental Pop', 'Instrumental Rock',
      'Ethnic', 'Gothic', 'Darkwave', 'Techno-Industrial', 'Electronic', 'Pop-Folk',
      'Eurodance', 'Dream', 'Southern Rock', 'Comedy', 'Cult', 'Gangsta',
      'Top 40', 'Christian Rap', 'Pop/Funk', 'Jungle', 'Native American', 'Cabaret',
      'New Wave', 'Psychadelic', 'Rave', 'Showtunes', 'Trailer', 'Lo-Fi',
      'Tribal', 'Acid Punk', 'Acid Jazz', 'Polka', 'Retro', 'Musical',
      'Rock & Roll', 'Hard Rock', 'Folk', 'Folk/Rock', 'National Folk', 'Swing',
      'Fast-Fusion', 'Bebob', 'Latin', 'Revival', 'Celtic', 'Bluegrass', 'Avantgarde',
      'Gothic Rock', 'Progressive Rock', 'Psychedelic Rock', 'Symphonic Rock', 'Slow Rock', 'Big Band',
      'Chorus', 'Easy Listening', 'Acoustic', 'Humour', 'Speech', 'Chanson',
      'Opera', 'Chamber Music', 'Sonata', 'Symphony', 'Booty Bass', 'Primus',
      'Porn Groove', 'Satire', 'Slow Jam', 'Club', 'Tango', 'Samba',
      'Folklore', 'Ballad', 'Power Ballad', 'Rhythmic Soul', 'Freestyle', 'Duet',
      'Punk Rock', 'Drum Solo', 'A capella', 'Euro-House', 'Dance Hall',
      'Goa', 'Drum & Bass', 'Club House', 'Hardcore', 'Terror',
      'Indie', 'BritPop', 'NegerPunk', 'Polsk Punk', 'Beat',
      'Christian Gangsta', 'Heavy Metal', 'Black Metal', 'Crossover', 'Contemporary C',
      'Christian Rock', 'Merengue', 'Salsa', 'Thrash Metal', 'Anime', 'JPop',
      'SynthPop' ];
      var form = document.getElementById('edit_form');
      form.style.display = 'block';
      var row = element.parent().parent();
      var tds = row.children();
      var id = parseInt(tds[0].innerText, 10);
      var song = scope.song;
      document.getElementById('edit_form_id').value = id;
      document.getElementById('remove_form_id').value = id;
      document.getElementsByName('title')[0].value = song.title;
      document.getElementsByName('artist_name')[0].value = song.artist;
      document.getElementsByName('album')[0].value = song.album;
      document.getElementsByName('tracknr')[0].value = song.tracknr;
      var genreId = genres.indexOf(song.genre);
      document.getElementsByName('genre')[0].value = genreId;
    })
  };
  return {
    restrict: 'A',
    link: link
  };
}).directive('hideParent', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      element.bind('click', function() {
        element.parent().css('display', 'none');
      })
    }
  };
}).directive('draggable', function($document) {
  return function(scope, element, attr) {
    var startX = 0, startY = 0, x = 600, y = 100;
    element.on('mousedown', function(event) {
        // Prevent default dragging of selected content
        event.preventDefault();
        startX = event.screenX - x;
        startY = event.screenY - y;
        $document.on('mousemove', mousemove);
        $document.on('mouseup', mouseup);
      });

    function mousemove(event) {
      y = event.screenY - startY;
      x = event.screenX - startX;
      element.parent().css({
        top: y + 'px',
        left:  x + 'px'
      });
    }

    function mouseup() {
      $document.unbind('mousemove', mousemove);
      $document.unbind('mouseup', mouseup);
    }
  };
}).directive('playSound', function($document) {
  return{
    restrict: 'A',
    link: function(scope, element, attr) {
      element.on('click', function() {
        var player = document.getElementById('player');
        function play(song) {
          var file = encodeURI('/file' + song.file);
          document.getElementById('song-title').innerHTML = song.artist + ' - ' + song.title;
          player.src = file;
          player.play();
        }
        play(scope.song);
        player.addEventListener('ended', function() {
          play(scope.song);
        });
      })
    }
  }
});
