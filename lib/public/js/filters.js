'use strict';
angular.module('jrtunesFilters', []).filter('album_path', function() {
  return function(input) {
    return encodeURI(input.replace('public/', ''));
  };
}).filter('by_id', function() {
  return function(songs, album_ids) {
    if(!songs || !album_ids)
    {
      return songs;
    }
    var array = [];
    for(var i = 0; i < songs.length; i ++)
    {
      if(album_ids.indexOf(songs[i].id) > -1)
      {
        array.push(songs[i]);
      }
    }
    return array;
  };
}).filter('shorter', function() {
  return function(input) {
    return input.length > 22 ? input.substring(0, 19) + '...' : input;
  }
}).filter('nameFilter', function () {
   return function(inputs,filter) {
      if(!filter)
      {
        return inputs;
      }
      var output = {};
      var filter = filter.toLowerCase();
      angular.forEach(inputs, function (input) {
        if (input.name.toLowerCase().indexOf(filter) > -1)
        {
             output[input.name] = input;
             console.log(output);
        }
       });
       return output.length === 0 ? inputs : output;
   };
}).filter('titleFilter', function () {
   return function(inputs,filter) {
      if(!filter)
      {
        return inputs;
      }
      var output = {};
      var filter = filter.toLowerCase();
      angular.forEach(inputs, function (input) {
        if (input.title.toLowerCase().indexOf(filter) > -1)
        {
           output[input.title] = input;
        }
       });
       return output.length === 0 ? inputs : output;
   };
});
