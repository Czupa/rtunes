# encoding: utf-8
require 'id3tag'
require 'mp3info'
class Song
  @@count = 0
  attr_accessor :title, :artist, :album, :tracknr, :genre
  attr_reader :id, :file
  def initialize(file)
    @@count += 1
    ID3Tag.read(File.open(file)) do |tag|
      @title = (tag.title ? tag.title.strip : 'Unknown title').freeze
      @artist = (tag.artist ? tag.artist.strip : 'Unknown artist').freeze
      @album = (tag.album ? tag.album.strip : 'Unknown album').freeze
      @tracknr = tag.track_nr ? tag.track_nr.freeze : nil
      @genre = tag.genre ? tag.genre.strip.freeze : nil
    end
    @id = @@count
    @file = File.absolute_path(file)
  end

  def self.res_count
    @@count = 0
  end

  def delete
    FileUtils.remove(@file, force: true)
  end

  def change_tag(tag, value)
    tag = tag.to_sym
    if tag == :genre
      Mp3Info.open(@file) do |mp3|
        mp3.tag.genre = value.to_i
      end
      @genre = GENRES[value.to_i]
    else
      Mp3Info.open(@file) do |mp3|
        mp3.tag.send("#{tag}=".freeze, value)
      end
      send("#{tag}=".freeze, value)
    end
  end

  def to_hash
    [:title, :artist, :album, :tracknr, :id, :file, :genre].
      each_with_object({}) do |i, h|
      if i == :tracknr
        h[:tracknr] = @tracknr.to_i > 0 ? @tracknr.to_i : @tracknr
      else
        h[i] = instance_variable_get("@#{i}".freeze)
      end
    end
  end
  GENRES = [
    'Blues', 'Classic Rock', 'Country', 'Dance', 'Disco', 'Funk',
    'Grunge', 'Hip-Hop', 'Jazz', 'Metal', 'New Age', 'Oldies',
    'Other', 'Pop', 'R&B', 'Rap', 'Reggae', 'Rock',
    'Techno', 'Industrial', 'Alternative', 'Ska', 'Death Metal', 'Pranks',
    'Soundtrack', 'Euro-Techno', 'Ambient', 'Trip-Hop', 'Vocal', 'Jazz+Funk',
    'Fusion', 'Trance', 'Classical', 'Instrumental', 'Acid', 'House',
    'Game', 'Sound Clip', 'Gospel', 'Noise', 'AlternRock', 'Bass',
    'Soul', 'Punk', 'Space', 'Meditative', 'Instrumental Pop', 'Instrumental Rock',
    'Ethnic', 'Gothic', 'Darkwave', 'Techno-Industrial', 'Electronic', 'Pop-Folk',
    'Eurodance', 'Dream', 'Southern Rock', 'Comedy', 'Cult', 'Gangsta',
    'Top 40', 'Christian Rap', 'Pop/Funk', 'Jungle', 'Native American', 'Cabaret',
    'New Wave', 'Psychadelic', 'Rave', 'Showtunes', 'Trailer', 'Lo-Fi',
    'Tribal', 'Acid Punk', 'Acid Jazz', 'Polka', 'Retro', 'Musical',
    'Rock & Roll', 'Hard Rock', 'Folk', 'Folk/Rock', 'National Folk', 'Swing',
    'Fast-Fusion', 'Bebob', 'Latin', 'Revival', 'Celtic', 'Bluegrass', 'Avantgarde',
    'Gothic Rock', 'Progressive Rock', 'Psychedelic Rock', 'Symphonic Rock', 'Slow Rock', 'Big Band',
    'Chorus', 'Easy Listening', 'Acoustic', 'Humour', 'Speech', 'Chanson',
    'Opera', 'Chamber Music', 'Sonata', 'Symphony', 'Booty Bass', 'Primus',
    'Porn Groove', 'Satire', 'Slow Jam', 'Club', 'Tango', 'Samba',
    'Folklore', 'Ballad', 'Power Ballad', 'Rhythmic Soul', 'Freestyle', 'Duet',
    'Punk Rock', 'Drum Solo', 'A capella', 'Euro-House', 'Dance Hall',
    'Goa', 'Drum & Bass', 'Club House', 'Hardcore', 'Terror',
    'Indie', 'BritPop', 'NegerPunk', 'Polsk Punk', 'Beat',
    'Christian Gangsta', 'Heavy Metal', 'Black Metal', 'Crossover', 'Contemporary C',
    'Christian Rock', 'Merengue', 'Salsa', 'Thrash Metal', 'Anime', 'JPop',
    'SynthPop' ].freeze
end
