require 'spec_helper'
require_relative '../lib/song'
describe Song do
  describe 'loading tags' do
    let(:song) { Song.new(File.dirname(__FILE__) + '/songs/InShape_-_Clarity.mp3') }
    subject { song }
    before { Song.res_count }
    its(:id) { should eq 1 }
    its(:title) { should eq 'Clarity' }
    its(:tracknr) { should eq '2' }
    its(:artist) { should eq 'InShape' }
    its(:album) { should eq 'REVERSIBLE' }
    its(:genre) { should eq 'Dance Hall' }
    its(:file) { should eq File.dirname(__FILE__) + '/songs/InShape_-_Clarity.mp3' }
  end
  describe '#change_tag' do
    let(:song) { Song.new(File.dirname(__FILE__) + '/songs/InShape_-_Clarity.mp3') }
    subject { song }
    context 'changing name' do
      before do
        song.change_tag(:title, 'Tak i nie')
      end

      after do
        Mp3Info.open(song.file) do |mp3|
          mp3.tag.title = "Clarity"
        end
      end 

      its(:title) { should eq 'Tak i nie' }

      it "changed tag too" do
        ID3Tag.read(File.open(song.file)) do |t|
          expect(t.title).to eq 'Tak i nie'
        end
      end
    end
    context 'changing genre' do
      before do
        song.change_tag(:genre, 1)
      end

      after do
        Mp3Info.open(song.file) do |mp3|
          mp3.tag.genre = 125
        end
      end

      its(:genre) { should eq 'Classic Rock' }

      it "changed tag too" do
        ID3Tag.read(File.open(song.file)) do |t|
          expect(t.genre).to eq 'Classic Rock'
        end
      end
    end
  end
  describe '#to_hash' do
    let(:song) { Song.new(File.dirname(__FILE__) + '/songs/InShape_-_Clarity.mp3') }
    subject { song.to_hash }
    its(:class) { should eq Hash }
    it 'shold have right values' do
      expect(subject[:title]).to eq song.title
      expect(subject[:album]).to eq song.album
      expect(subject[:artist]).to eq song.artist
      expect(subject[:tracknr].to_s).to eq song.tracknr.to_s
      expect(subject[:id]).to eq song.id
      expect(subject[:genre]).to eq song.genre
      expect(subject[:file]).to eq song.file
    end
  end
end