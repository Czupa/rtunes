Gem::Specification.new do |s|
  s.name          = 'jrtunes'
  s.version       = '1.2.0'
  s.date          = '2014-04-22'
  s.summary       = "Just project for school"
  s.description   = "Umm; simple mp3 tag editor and player based on sinatra"
  s.authors       = ["Marcin Henryk Bartkowiak"]
  s.email         = 'mhbartkowiak@gmail.com'
  s.files         = `git ls-files`.split("\n")
  s.test_files    = `git ls-files -- {test,spec,features}/*`.split("\n")
  s.executables   = ['jrtunes']
  s.require_paths = ['lib']
  s.license       = 'MIT'
  s.platform      = 'java'
  s.add_dependency "ruby-mp3info", '~> 0.8', '>= 0.8.3'
  s.add_dependency 'id3tag'
  s.add_dependency 'haml'
  s.add_dependency 'puma'
  s.add_dependency 'sinatra'
end