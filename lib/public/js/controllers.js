'use strict';

var jrtunesControllers = angular.module('jrtunesControllers', []);

jrtunesControllers.controller('SongListCtrl', ['$scope', 'Library',
  function($scope, Library) {
    Library.query(function(data) {
      $scope.albums = data.albums;
      $scope.songs = data.songs || [];
      $scope.artists = data.artists;
      $scope.genres = data.genres;
    });
    $scope.sort = {
      column: '',
      descending: false
    };
    if(!$scope.id_filter) {
      $scope.id_filter = {};
    }
    $scope.changeSorting = function(column) {
      var sort = $scope.sort;

      if(sort.column === column) {
          sort.descending = !sort.descending;
      }
      else if(sort.column === '' && column === 'id') {
        sort.column = column;
        sort.descending = true;
      }
      else {
          sort.column = column;
          sort.descending = false;
      }
    };
    $scope.selectedHeader = function(column) {
        return column === $scope.sort.column && 'sort-' + $scope.sort.descending;
    };

    $scope.containsComparator = function(expected, actual){
      return actual.indexOf(expected) > -1;
    };
    $scope.toggleFilter = function(neew, old, album) {
      if(!old)
      {
        if(album)
        {
          $scope.sort.column = 'tracknr';
        }
        return neew;
      }
      else if(old === neew)
      {
        $scope.sort.column = '';
        $scope.sort.descending = false;
        return '';
      }
      else
      {
        if(album)
        {
          $scope.sort.column = 'tracknr';
        }
        return neew;
      }
    };
  }]);
