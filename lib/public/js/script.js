function apdejt() {
  var element = document.getElementById("wart");
  var war = parseInt(element.getAttribute('data-value'), 10);
  element.innerHTML = tajm_perser(war + 1);
  element.setAttribute('data-value', war + 1);
}
function tajm_perser(valju) {
  var string = '';
  var lel = true; var jej = true;
  if(valju >= 3600)
  {
    var godz;
    if(Math.floor(valju/3600) === 1)
    {
      godz = 'godzinę';
    }
    else if(Math.floor(valju/3600) === 2 || Math.floor(valju/3600) === 3 || Math.floor(valju/3600) === 4)
    {
      godz = 'godziny';
    }
    else
    {
      godz = 'godzin';
    }
    string += (Math.floor(valju/3600)) + ' ' + godz + ' ';
    valju = valju % 3600;
  }
  if(valju >= 60)
  {
    var min;
    if(Math.floor(valju/60) === 1)
    {
      min = 'minutę';
    }
    else if(Math.floor(valju/60) === 2 || Math.floor(valju/60) === 3 || Math.floor(valju/60) === 4)
    {
      min = 'minuty';
    }
    else
    {
      min = 'minut';
    }
    string += (Math.floor(valju/60)) + ' ' + min + ' ';
    valju = valju % 60;
  }
  if(valju >= 0)
  {
    var sek;
    if(Math.floor(valju) === 1)
    {
      sek = 'sekundę';
    }
    else if(Math.floor(valju) ===  2 || Math.floor(valju) === 3 || Math.floor(valju) === 4)
    {
      sek = 'sekundy';
    }
    else
    {
      sek = 'sekund';
    }
    string += (valju) + ' ' + sek;
  }
  return string + '.';
}
setInterval(function(){ apdejt(); }, 1000);
