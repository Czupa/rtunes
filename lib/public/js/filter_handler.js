function hasClass(element, cls) {
  return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
}

function removeChecked(name)
{
  var elements = document.getElementsByName(name);
  for (var i=0, len=elements.length; i<len; ++i)
    if (elements[i].checked) return elements[i].checked = false;
}
var checkedradio;
function toggleSelected(element) {
  var parent = element.parentNode;
  var already_selected = document.getElementsByClassName('selected')[0];
  if(already_selected && already_selected != parent)
  {
    already_selected.classList.remove('selected');
  }
  checkedradio = null
  removeChecked('artist');
  parent.classList.toggle('selected');
}

function docheck(thisradio) {
  if (checkedradio == thisradio) {
    thisradio.checked = false;
    checkedradio = null;
  }
  else {checkedradio = thisradio;}
  var already_selected = document.getElementsByClassName('selected')[0];
  if(already_selected && already_selected != parent)
  {
    already_selected.classList.remove('selected');
  }
}
