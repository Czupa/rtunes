require 'set'
require 'yaml'
require_relative 'song'
class Library
  attr_reader :songs, :albums, :genres, :artists, :config

  CONFIG_LOCATION = File.join(File.dirname(__FILE__), 'config.yml').freeze

  def initialize(directory = nil)
    load_config
    @config[:dir] = directory if directory
    @config ||= {}
    @config[:dir] ||= '/dev/null'
    save_config
    @dir = @config[:dir]
    load_songs
  end

  def load_config
    if File.file? CONFIG_LOCATION
      @config = YAML.load_file(CONFIG_LOCATION)
    else
      File.new(File.expand_path(CONFIG_LOCATION), 'w').close
      @config = YAML.load_file(CONFIG_LOCATION)
    end
  end

  def save_config
    File.open(CONFIG_LOCATION, 'w') do |f|
      f.write @config.to_yaml
    end
  end

  def load_songs
    FileUtils.rm_rf("#{File.dirname(__FILE__)}/public/albums/.", secure: true)
    @songs = []; @song_hash = []
    @artists = {}; @albums = {}; @genres = {}
    Song.res_count
    Dir["#{@dir}/**/*.mp3"].each do |s|
      song = Song.new(s)
      @songs << song
      [:genre, :album, :artist].each { |n| set_tag(song, n) }
    end
  end

  def push_id(var, song, song_value)
    var[song_value] ||= {}
    var[song_value][:songs] ||= []
    var[song_value][:songs] << song.id
  end

  def set_tag(song, tag)
    var = instance_variable_get("@#{tag}s")
    song_value = song.public_send(tag)
    if song_value && !song_value.empty?
      push_id(var, song, song_value)
      if tag == :album
        var[song_value][:title] ||= song.album
        var[song_value][:cover] ||= avatar(song)
      elsif song.album
        var[song_value][:albums] ||= []
        unless var[song_value][:albums].include?(song.album)
          var[song_value][:albums] << song.album
        end
      end
    end
  end

  def avatar(song)
    Mp3Info.open(song.file) do |f|
      begin
        a = f.tag2.pictures[0]
        ext = File.extname a[0]
        str_path = File.dirname(__FILE__) + "/public/albums/#{song.album}#{ext}".freeze
        str = "/public/albums/#{song.album}#{ext}"
        path_len = File.dirname(__FILE__).length
        while File.exists? str_path
          str_path.insert(path_len + 15, 'a')
          str.insert(15, 'a')
        end
        File.open(str_path, 'w') do |av|
          av.write a[1]
        end
        str
      rescue
        '/img/music-icon.png'.freeze
      end
    end
  end

  #def save
    #@config[:genres] = @genres
    #@config[:albums] = @albums
    #@config[:artists] = @artists
    #@config[:songs] = @songs
    #@config[:song_hash] = @song_hash
    #File.open(File.dirname(__FILE__) + '/config.yml', 'w') do |f|
      #f.write @config.to_yaml
    #end
  #end
end
