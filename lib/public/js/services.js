'use strict';

/* Services */

var jrtunesServices = angular.module('jrtunesServices', ['ngResource']);

jrtunesServices.factory('Library', ['$resource',
  function($resource){
    return $resource('/library', {}, {
      query: {method:'GET', isArray: false}
    });
  }]);
